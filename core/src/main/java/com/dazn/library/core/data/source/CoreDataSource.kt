package com.dazn.library.core.data.source

import com.dazn.library.core.data.Event
import com.dazn.library.core.network.Result

interface CoreDataSource {

    suspend fun getEvents(): Result<List<Event>>

    suspend fun getScheduledEvents(): Result<List<Event>>

}