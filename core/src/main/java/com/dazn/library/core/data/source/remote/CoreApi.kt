package com.dazn.library.core.data.source.remote

import com.dazn.library.core.data.Event
import retrofit2.Response
import retrofit2.http.GET

interface CoreApi {

    @GET("getEvents")
    suspend fun getEvents(): Response<List<Event>>

    @GET("getSchedule")
    suspend fun getScheduledEvents(): Response<List<Event>>

}