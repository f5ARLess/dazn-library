package com.dazn.library.core.data

import android.content.Context
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dazn.library.core.utils.TimeUtils
import java.util.*

@Entity
class Event(@PrimaryKey
            var id: String = "",
            var title: String = "",
            var subtitle: String = "",
            var date: Date? = null,
            var imageUrl: String = "",
            var videoUrl: String = "") {

    fun getFormattedDate(context: Context): String =
        if (date != null) TimeUtils.timeStampToHumanDate(context, date!!, false) else ""

    fun getScheduledDate(context: Context): String = if (date != null) TimeUtils.timeStampToHumanDate(context, date!!) else ""

    override fun toString(): String = "Event: $id\nTitle = $title\nSubtitle = $subtitle\nDate = $date\nImage URL = $imageUrl\nVideo URL = $videoUrl"
}