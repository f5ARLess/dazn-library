package com.dazn.library.core.data.source.remote

import android.content.Context
import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.internal.http.HttpHeaders
import okhttp3.logging.HttpLoggingInterceptor
import okio.Buffer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.io.EOFException
import java.io.IOException
import java.nio.charset.Charset
import java.nio.charset.UnsupportedCharsetException
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class CoreRetrofit {
    companion object {
        var retrofit: Retrofit? = null

        @JvmStatic
        fun getInstance(): Retrofit {
            if (retrofit == null) {
                val timberLoggingInterceptor = TimberLoggingInterceptor()
                timberLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

                val okHttpClientBuilder = OkHttpClient.Builder().addInterceptor(timberLoggingInterceptor)

                // Create a trust manager that does not validate certificate chains
                val trustAllCerts:  Array<TrustManager> = arrayOf(object : X509TrustManager {
                    override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?){}
                    override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {}
                    override fun getAcceptedIssuers(): Array<X509Certificate>  = arrayOf()
                })

                // Install the all-trusting trust manager
                val  sslContext = SSLContext.getInstance("SSL")
                sslContext.init(null, trustAllCerts, SecureRandom())

                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory = sslContext.socketFactory
                if (trustAllCerts.isNotEmpty() &&  trustAllCerts.first() is X509TrustManager) {
                    okHttpClientBuilder.sslSocketFactory(sslSocketFactory, trustAllCerts.first() as X509TrustManager)
                    okHttpClientBuilder.hostnameVerifier { _, _ -> true }
                }

                val okHttpClient = okHttpClientBuilder.build()

                val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create()

                retrofit = Retrofit.Builder()
                    .baseUrl("https://us-central1-dazn-sandbox.cloudfunctions.net/")
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            }
            return retrofit!!
        }

        @JvmStatic
        fun clear() {
            retrofit = null
        }
    }

    internal class TimberLoggingInterceptor : Interceptor {
        private val UTF8 = Charset.forName("UTF-8")
        var level = HttpLoggingInterceptor.Level.NONE

        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val level: HttpLoggingInterceptor.Level = this.level

            val request = chain.request()
            if (level == HttpLoggingInterceptor.Level.NONE) {
                return chain.proceed(request)
            }

            val logBody = level == HttpLoggingInterceptor.Level.BODY
            val logHeaders = logBody || level == HttpLoggingInterceptor.Level.HEADERS

            val requestBody = request.body()
            val hasRequestBody = requestBody != null

            val connection = chain.connection()
            val protocol = if (connection != null) connection.protocol() else Protocol.HTTP_1_1
            var requestStartMessage = "--> " + request.method() + ' ' + request.url() + ' ' + protocol
            if (!logHeaders && hasRequestBody) {
                requestStartMessage += " (" + requestBody!!.contentLength() + "-byte body)"
            }
            Timber.d(requestStartMessage)

            if (logHeaders) {
                if (hasRequestBody) { // Request body headers are only present when installed as a network interceptor. Force
// them to be included (when available) so there values are known.
                    if (requestBody!!.contentType() != null) {
                        Timber.d("Content-Type: %s", requestBody.contentType())
                    }
                    if (requestBody.contentLength() != -1L) {
                        Timber.d("Content-Length: %s", requestBody.contentLength())
                    }
                }
                val headers = request.headers()
                var i = 0
                val count = headers.size()
                while (i < count) {
                    val name = headers.name(i)
                    // Skip headers from the request body as they are explicitly logged above.
                    if (!"Content-Type".equals(name, ignoreCase = true) && !"Content-Length".equals(name, ignoreCase = true)) {
                        Timber.d("%s : %s", name, headers.value(i))
                    }
                    i++
                }
                if (!logBody || !hasRequestBody) {
                    Timber.d("--> END %s", request.method())
                } else if (bodyEncoded(request.headers())) {
                    Timber.d("--> END %s (encoded body omitted)", request.method())
                } else {
                    val buffer = Buffer()
                    requestBody!!.writeTo(buffer)
                    var charset = UTF8
                    val contentType = requestBody.contentType()
                    if (contentType != null) {
                        charset = contentType.charset(UTF8)
                    }
                    Timber.d("")
                    if (isPlaintext(buffer)) {
                        Timber.d(buffer.readString(charset))
                        Timber.d("--> END %s (%s-byte body)", request.method(), requestBody.contentLength())
                    } else {
                        Timber.d("--> END %s (binary %s-byte body omitted)", request.method(), requestBody.contentLength())
                    }
                }
            }

            val startNs = System.nanoTime()
            val response: Response = try {
                chain.proceed(request)
            } catch (e: Exception) {
                Timber.d("<-- HTTP FAILED: $e")
                throw e
            }
            val tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs)

            val responseBody = response.body()
            val contentLength = responseBody!!.contentLength()
            val bodySize = if (contentLength != -1L) "$contentLength-byte" else "unknown-length"
            Timber.d("<-- " + response.code() + ' ' + response.message() + ' '
                    + response.request().url() + " (" + tookMs + "ms" + (if (!logHeaders) ", "
                    + bodySize + " body" else "") + ')')

            if (logHeaders) {
                val headers = response.headers()
                var i = 0
                val count = headers.size()
                while (i < count) {
                    Timber.d("${headers.name(i)} : ${headers.value(i)}")
                    i++
                }
                if (!logBody || !HttpHeaders.hasBody(response)) {
                    Timber.d("<-- END HTTP")
                } else if (bodyEncoded(response.headers())) {
                    Timber.d("<-- END HTTP (encoded body omitted)")
                } else {
                    val source = responseBody.source()
                    source.request(Long.MAX_VALUE) // Buffer the entire body.
                    val buffer = source.buffer()
                    var charset = UTF8
                    val contentType = responseBody.contentType()
                    if (contentType != null) {
                        charset = try {
                            contentType.charset(UTF8)
                        } catch (e: UnsupportedCharsetException) {
                            Timber.d("")
                            Timber.d("Couldn't decode the response body; charset is likely malformed.")
                            Timber.d("<-- END HTTP")
                            return response
                        }
                    }
                    if (!isPlaintext(buffer)) {
                        Timber.d("")
                        Timber.d("<-- END HTTP (binary ${buffer.size()}-byte body omitted)")
                        return response
                    }
                    if (contentLength != 0L) {
                        Timber.d("")
                        Timber.d(buffer.clone().readString(charset))
                    }
                    Timber.d("<-- END HTTP (${buffer.size()}-byte body)")
                }
            }

            return response
        }

        fun isPlaintext(buffer: Buffer): Boolean {
            return try {
                val prefix = Buffer()
                val byteCount = if (buffer.size() < 64) buffer.size() else 64
                buffer.copyTo(prefix, 0, byteCount)
                for (i in 0..15) {
                    if (prefix.exhausted()) {
                        break
                    }
                    val codePoint = prefix.readUtf8CodePoint()
                    if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                        return false
                    }
                }
                true
            } catch (e: EOFException) {
                false // Truncated UTF-8 sequence.
            }
        }

        private fun bodyEncoded(headers: Headers): Boolean {
            val contentEncoding = headers["Content-Encoding"]
            return contentEncoding != null && !contentEncoding.equals("identity", ignoreCase = true)
        }
    }

}