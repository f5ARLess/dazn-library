package com.dazn.library.core.data.source.local

import androidx.room.TypeConverter
import java.io.File
import java.util.*

class Converters {
    /**
     * Date Convert
     * **/
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    /**
     * File Convert
     * **/
    @TypeConverter
    fun fromPath(value: String?): File? {
        return value?.let { File(it) }
    }

    @TypeConverter
    fun getFilePath(location: File?): String? {
        return location?.path
    }
}