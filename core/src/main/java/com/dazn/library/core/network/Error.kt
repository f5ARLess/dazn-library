package com.dazn.library.core.network

data class Error(var code:Int, var message:String)