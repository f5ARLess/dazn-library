package com.dazn.library.core.network

open class Result<T> private constructor(val status: Status, val data: T?, val resultError: ResultError?) {
    enum class Status {
        SUCCESS, ERROR, LOADING
    }
    companion object {
        fun <T> success(data: T?): Result<T> {
            return Result(Status.SUCCESS, data, null)
        }
        fun <T> error(resultError: ResultError?): Result<T> {
            return Result(Status.ERROR, null, resultError)
        }
        fun <T> loading(data: T? = null): Result<T> {
            return Result(Status.LOADING, data, null)
        }
    }

    fun isSuccess(): Boolean = status == Status.SUCCESS
}