package com.dazn.library.core.data.source.local

import androidx.lifecycle.LiveData
import com.dazn.library.core.data.Event
import androidx.room.*

@Dao
interface EventDao {

    @Query("SELECT * FROM Event")
    suspend fun getAllEvents(): List<Event>


    @Query("SELECT * FROM Event WHERE id = :id")
    fun getEvent(id: Int): LiveData<Event>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(event: Event)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(event: List<Event>)

}