package com.dazn.library.core.feed

enum class FeedFilterType {
    /**
     * Normal events
     */
    HOME,

    /**
     * Scheduled events, different date formatting and refresh periodically
     */
    SCHEDULED

}