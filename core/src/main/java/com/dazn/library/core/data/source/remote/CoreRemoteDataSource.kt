package com.dazn.library.core.data.source.remote

import com.dazn.library.core.data.Event
import com.dazn.library.core.data.source.CoreDataSource
import com.dazn.library.core.network.Result
import timber.log.Timber
import javax.inject.Inject


class CoreRemoteDataSource @Inject constructor(private val coreApi: CoreApi): BaseRemoteDataSource(), CoreDataSource {

    override suspend fun getEvents(): Result<List<Event>> = safeApiCall(call = { coreApi.getEvents() })
        .also { Timber.d("Result = $it")  }

    override suspend fun getScheduledEvents(): Result<List<Event>> = safeApiCall(call = { coreApi.getScheduledEvents() })
        .also { Timber.d("Result = $it")  }

}