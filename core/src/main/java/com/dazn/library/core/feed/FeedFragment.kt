package com.dazn.library.core.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dazn.library.core.databinding.FragmentFeedBinding
import com.dazn.library.core.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FeedFragment: Fragment() {

    private var binding: FragmentFeedBinding by autoCleared()
    private val viewModel: FeedViewModel by viewModels()
    private lateinit var adapter: FeedAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager

    companion object {
        const val FILTER = "FeedFragment.FILTER"

        fun newInstance(): FeedFragment = FeedFragment().apply {
            arguments = bundleOf(FILTER to FeedFilterType.HOME)
        }

        fun newScheduleInstance(): FeedFragment = FeedFragment().apply {
            arguments = bundleOf(FILTER to FeedFilterType.SCHEDULED)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFeedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = FeedAdapter(viewModel.currentFilter)
        linearLayoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.layoutManager = linearLayoutManager
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && viewModel.isLoading.not()) {
                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == adapter.itemCount-1) {
                        viewModel.loadEvents()
                    }
                }
            }

        })

        viewModel.events.observe(viewLifecycleOwner, {
            if (it.isNullOrEmpty().not()) {
                adapter.setEvents(it)
                binding.recyclerView.visibility = View.VISIBLE
                binding.emptyPlaceHolder.visibility = View.GONE
            }
        })

        viewModel.newEvents.observe(viewLifecycleOwner, {
            if (it.isNullOrEmpty().not()) {
                val holder = binding.recyclerView.findViewHolderForAdapterPosition(adapter.itemCount-1) as FeedAdapter.FeedViewHolder
                adapter.addEvents(it)
                binding.recyclerView.post { holder.hideProgressbar() }
            }
        })

        viewModel.swipeRefreshLoading.observe(viewLifecycleOwner, {
            binding.swipeRefreshLayout.isRefreshing = it
        })

        viewModel.toastMessage.observe(viewLifecycleOwner, {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.loadEvents(true)
        }

    }
}