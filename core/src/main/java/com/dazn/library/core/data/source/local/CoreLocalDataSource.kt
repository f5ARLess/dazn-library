package com.dazn.library.core.data.source.local

import com.dazn.library.core.data.Event
import com.dazn.library.core.data.source.CoreDataSource
import com.dazn.library.core.network.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class CoreLocalDataSource @Inject constructor(private val coreDatabase: CoreDatabase,
                                              private val dispatcher: CoroutineDispatcher = Dispatchers.IO): CoreDataSource {
    private val eventDao = coreDatabase.eventDao()

    override suspend fun getEvents(): Result<List<Event>> = withContext(dispatcher) {
        Result.success(eventDao.getAllEvents()).also { Timber.d("Result = $it")  }
    }

    override suspend fun getScheduledEvents(): Result<List<Event>> = withContext(dispatcher) {
        Result.success(eventDao.getAllEvents()).also { Timber.d("Result = $it")  }
    }

}