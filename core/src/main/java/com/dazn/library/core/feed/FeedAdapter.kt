package com.dazn.library.core.feed

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dazn.library.core.R
import com.dazn.library.core.data.Event
import com.dazn.library.core.databinding.AdapterFeedBinding

class FeedAdapter(val filter: FeedFilterType): RecyclerView.Adapter<FeedAdapter.FeedViewHolder>() {
    private var events = arrayListOf<Event>()
    private lateinit var context: Context

    fun setEvents(events: List<Event>) {
        this.events.clear()
        this.events.addAll(events)
        notifyDataSetChanged()
    }

    fun addEvents(events: List<Event>) {
//        sortAllEvents(events) //uncomment this if you want all event to be sorted after adding new events. But this will make app flicker
        val positionStart = this.events.size
        this.events.addAll(events)
        notifyItemRangeInserted(positionStart, events.size)
    }

    //Don't use this
    private fun sortAllEvents(events: List<Event>) {
        val unsortedEvents = arrayListOf<Event>()
        unsortedEvents.addAll(this.events)
        unsortedEvents.addAll(events)
        this.events.clear()
        this.events.addAll(unsortedEvents.sortedBy { it.date })
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedViewHolder {
        val binding: AdapterFeedBinding = AdapterFeedBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        context = parent.context
        return FeedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) = holder.bind(events[position])

    override fun getItemCount(): Int = events.size

    inner class FeedViewHolder(private val adapterFeedBinding: AdapterFeedBinding): RecyclerView.ViewHolder(adapterFeedBinding.root) {

        private lateinit var event: Event

        init {
//        adapterFeedBinding.root.setOnClickListener(this)
        }

        @SuppressLint("SetTextI18n")
        fun bind(event: Event) {
            this.event = event
            adapterFeedBinding.title.text = event.title
            adapterFeedBinding.subtitle.text = event.subtitle
            adapterFeedBinding.date.text = when(filter) {
                FeedFilterType.HOME -> event.getFormattedDate(context)
                FeedFilterType.SCHEDULED -> event.getScheduledDate(context)
            }
            adapterFeedBinding.subtitle.visibility = if (event.subtitle.isEmpty()) View.GONE else View.VISIBLE
            adapterFeedBinding.progressBar.visibility = if (adapterPosition == events.size-1) View.VISIBLE else View.GONE
            Glide.with(adapterFeedBinding.root)
                .load(event.imageUrl)
                .centerInside()
                .placeholder(R.drawable.dazn_placeholder)
                .into(adapterFeedBinding.image)
        }

        fun hideProgressbar() {
            adapterFeedBinding.progressBar.visibility = View.GONE
        }

//    override fun onClick(v: View?) {
//        listener.onClickedCharacter(character.id)
//    }
    }
}

