package com.dazn.library.core.utils

import android.content.Context
import android.text.format.DateUtils
import android.text.format.DateUtils.FORMAT_SHOW_YEAR
import com.dazn.library.core.R
import java.text.SimpleDateFormat
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.abs

object TimeUtils {

    fun timeStampToHumanDate(context: Context, date: Date, supportTomorrow: Boolean = true): String {
        val calendar = Calendar.getInstance()
        calendar.time = date
        return timeStampToHumanDate(context, calendar, supportTomorrow)
    }

    private fun timeStampToHumanDate(context: Context, calendar: Calendar, supportTomorrow: Boolean = true): String {
        val time = DateUtils.formatDateTime(context, calendar.timeInMillis, DateUtils.FORMAT_SHOW_TIME or DateUtils.FORMAT_24HOUR)
        val dateFormat = SimpleDateFormat("MM.dd.yyyy", Locale.getDefault())
        return when {
            isToday(calendar) -> context.getString(R.string.today) + ", $time"
            isYesterday(calendar) -> context.getString(R.string.yesterday) + ", $time"
            supportTomorrow -> {
                val daysDifference = getDaysDifference(calendar)
                when {
                    isTomorrow(calendar) -> context.getString(R.string.tomorrow) + ", $time"
                    daysDifference > 0 -> context.getString(R.string.in_number_of_day, daysDifference.toString())
                    daysDifference < 0 -> context.getString(R.string.days_ago, abs(daysDifference).toString())
                    else -> dateFormat.format(calendar.time)
                }
            }
            else -> {
                val daysDifference = getDaysDifference(calendar)
                if (daysDifference < 0) context.getString(R.string.days_ago, abs(daysDifference).toString())
                else dateFormat.format(calendar.time)
            }
        }
    }

    private fun isToday(calendar: Calendar): Boolean = isSameDay(calendar, Calendar.getInstance());

    private fun isYesterday(calendar: Calendar): Boolean {
        val yesterday: Calendar = Calendar.getInstance()
        yesterday.roll(Calendar.DAY_OF_MONTH, -1)
        return isSameDay(calendar, yesterday);
    }

    private fun isTomorrow(calendar: Calendar): Boolean {
        val tomorrow: Calendar = Calendar.getInstance()
        tomorrow.roll(Calendar.DAY_OF_MONTH, 1)
        return isSameDay(calendar, tomorrow);
    }

    private fun getDaysDifference(calendar: Calendar) =
        TimeUnit.MILLISECONDS.toDays(calendar.timeInMillis - Calendar.getInstance().timeInMillis)

    fun isSameDay(calendar1: Calendar?, calendar2: Calendar?): Boolean =
        if (calendar1 == null || calendar2 == null) {
            false
        } else {
            calendar1[Calendar.ERA] == calendar2[Calendar.ERA] && calendar1[Calendar.YEAR] == calendar2[Calendar.YEAR] &&
                    calendar1[Calendar.DAY_OF_YEAR] == calendar2[Calendar.DAY_OF_YEAR]
        }
}