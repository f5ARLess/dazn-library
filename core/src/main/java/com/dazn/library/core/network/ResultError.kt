package com.dazn.library.core.network

import com.dazn.library.core.network.Error

class ResultError(val errorMessage: String = "") {

    var error: Error = Error(-999, errorMessage)

}