package com.dazn.library.core.feed

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dazn.library.core.data.Event
import com.dazn.library.core.data.source.CoreRepository
import com.dazn.library.core.feed.FeedFragment.Companion.FILTER
import com.dazn.library.core.utils.wrapEspressoIdlingResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class FeedViewModel @Inject constructor(private val coreRepository: CoreRepository, savedStateHandle: SavedStateHandle): ViewModel() {
    val newEvents: MutableLiveData<List<Event>> = MutableLiveData()
    val events: MutableLiveData<List<Event>> = MutableLiveData()
    val swipeRefreshLoading: MutableLiveData<Boolean> = MutableLiveData()
    val toastMessage: MutableLiveData<String> = MutableLiveData()
    var isLoading: Boolean = false
    var currentFilter = FeedFilterType.HOME

    companion object {

        private const val periodicRefresh: Long = (30 * 1000).toLong()

    }

    init {
        val filter: FeedFilterType? = savedStateHandle[FILTER]
        filter?.let {
            this.currentFilter = it
        }

        loadEvents(true)
        if (filter == FeedFilterType.SCHEDULED) {
            launchPeriodicalRefresh()
        }
    }

    fun loadEvents(forceFetch: Boolean = false) {
        wrapEspressoIdlingResource {
            updateLoading(forceFetch, true)
            viewModelScope.launch {
                val result = when (currentFilter) {
                    FeedFilterType.HOME -> coreRepository.getEvents()
                    FeedFilterType.SCHEDULED -> coreRepository.getScheduledEvents()
                }
                if (result.isSuccess()) {
                    val eventsResult = result.data?.sortedBy { it.date }
                    if (forceFetch) {
                        events.value = eventsResult
                    } else {
                        newEvents.value = eventsResult
                    }
                } else {
                    toastMessage.value = result.resultError?.errorMessage
                }
                updateLoading(forceFetch, false)
            }
        }
    }

    private fun launchPeriodicalRefresh() {
        Timber.d("Launching periodic refresh every ${periodicRefresh/1000} seconds")
        viewModelScope.launch {
            while(isActive) {
                delay(periodicRefresh)
                Timber.d("Auto refreshing..")
                loadEvents(true)
            }
        }
    }

    private fun updateLoading(forceFetch: Boolean, isLoading: Boolean) {
        this.isLoading = isLoading
        if (forceFetch) {
            swipeRefreshLoading.value = isLoading
        }
    }

}