package com.dazn.library.core.data.source

import com.dazn.library.core.data.Event
import com.dazn.library.core.data.source.local.CoreLocalDataSource
import com.dazn.library.core.data.source.remote.CoreRemoteDataSource
import com.dazn.library.core.di.CoreModule
import com.dazn.library.core.network.Result
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import timber.log.Timber
import javax.inject.Inject

/**
 * No need to get/save events from/in persistence for now. Directly fetch from remote
 */
class CoreRepository @Inject constructor(@CoreModule.CoreLocalDataSource private val coreLocalDataSource: CoreDataSource,
                                         @CoreModule.CoreRemoteDataSource private val coreRemoteDataSource: CoreDataSource,
                                         private val dispatcher: CoroutineDispatcher = Dispatchers.IO): CoreDataSource {

    override suspend fun getEvents(): Result<List<Event>> {
        Timber.d("getEvents()")
        return coreRemoteDataSource.getEvents().also { Timber.d("Result = $it") }
    }

    override suspend fun getScheduledEvents(): Result<List<Event>> {
        Timber.d("getScheduledEvents()")
        return coreRemoteDataSource.getScheduledEvents().also { Timber.d("Result = $it") }
    }
}