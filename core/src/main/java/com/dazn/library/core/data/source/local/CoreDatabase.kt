package com.dazn.library.core.data.source.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dazn.library.core.data.Event

@Database(entities = [Event::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class CoreDatabase : RoomDatabase() {

    abstract fun eventDao(): EventDao

    companion object {
        @Volatile private var instance: CoreDatabase? = null

        fun getDatabase(context: Context): CoreDatabase =
            instance ?: synchronized(this) { instance ?: buildDatabase(context).also { instance = it } }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, CoreDatabase::class.java, "daznprototypedb")
                .fallbackToDestructiveMigration()
                .build()

        fun clear() {
            instance = null
        }
    }

}