package com.dazn.library.core.data.source.remote

import com.dazn.library.core.network.Error
import com.dazn.library.core.network.Result
import com.dazn.library.core.network.ResultError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import timber.log.Timber
import java.net.SocketTimeoutException

open class BaseRemoteDataSource {

    protected suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): Result<T> {
        return try {
            val response = call.invoke()
            if (response.isSuccessful) {
                Timber.d("onSuccess() = ${response.body().toString()}")
                Result.success(response.body())
            } else {
                val resultError = ResultError()
                val errorMessage = withContext(Dispatchers.IO) {
                    response.errorBody()?.string().toString()
                }
                Timber.d("onFailure() = $errorMessage")
                resultError.error = Error(response.code(), errorMessage)
                Result.error(resultError)
            }
        } catch (se3: SocketTimeoutException) {
            Result.error(ResultError(se3.localizedMessage.toString()))
        } catch (e: Throwable) {
            Result.error(ResultError("No internet connection"))
        }
    }

}