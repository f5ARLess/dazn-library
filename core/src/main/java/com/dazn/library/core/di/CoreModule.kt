package com.dazn.library.core.di

import android.content.Context
import com.dazn.library.core.data.source.CoreDataSource
import com.dazn.library.core.data.source.CoreRepository
import com.dazn.library.core.data.source.local.CoreDatabase
import com.dazn.library.core.data.source.local.CoreLocalDataSource
import com.dazn.library.core.data.source.remote.CoreApi
import com.dazn.library.core.data.source.remote.CoreRemoteDataSource
import com.dazn.library.core.data.source.remote.CoreRetrofit
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CoreModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class CoreRemoteDataSource

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class CoreLocalDataSource

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = CoreRetrofit.getInstance()

    @Provides
    fun provideCoreApi(retrofit: Retrofit): CoreApi = retrofit.create(CoreApi::class.java)

    @Singleton
    @Provides
    @CoreRemoteDataSource
    fun provideCoreRemoteDataSource(coreApi: CoreApi): CoreDataSource = CoreRemoteDataSource(coreApi)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = CoreDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    @CoreLocalDataSource
    fun provideCoreLocalDataSource(coreDatabase: CoreDatabase): CoreDataSource = CoreLocalDataSource(coreDatabase)

    @Singleton
    @Provides
    fun provideIoDispatcher() = Dispatchers.IO

}

@Module
@InstallIn(SingletonComponent::class)
abstract class RepoModule {

    @Singleton
    @Binds
    abstract fun bindRepo(repo: CoreRepository): CoreDataSource
}
