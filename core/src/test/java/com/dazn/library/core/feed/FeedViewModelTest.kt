package com.dazn.library.core.feed

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import com.dazn.library.core.LiveDataTestUtil
import com.dazn.library.core.MainCoroutineRule
import com.dazn.library.core.data.source.CoreRepository
import com.dazn.library.core.data.source.FakeDataSource
import com.dazn.library.core.data.source.FakeEvents
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule

import org.junit.Test

@ExperimentalCoroutinesApi
class FeedViewModelTest {
    private val events = FakeEvents.get()
    private val scheduledEvents = FakeEvents.getScheduled()

    private lateinit var coreRemoteDataSource: FakeDataSource
    private lateinit var coreLocalDataSource: FakeDataSource
    private lateinit var coreRepository: CoreRepository

    // Subjects under test
    private lateinit var feedViewModel: FeedViewModel
    private lateinit var scheduledEventsViewModel: FeedViewModel

    //This make unit tests stuck not sure why. Will debug in the future
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Before
    fun setupViewModel() {
        coreRemoteDataSource = FakeDataSource(events, scheduledEvents)
        coreLocalDataSource = FakeDataSource()
        coreRepository = CoreRepository(coreLocalDataSource, coreRemoteDataSource)
        feedViewModel = FeedViewModel(coreRepository, SavedStateHandle())
        val savedStateHandle = SavedStateHandle()
        savedStateHandle.set(FeedFragment.FILTER, FeedFilterType.SCHEDULED)
        scheduledEventsViewModel = FeedViewModel(coreRepository, savedStateHandle)
    }

    @Test
    fun loadEvents_forceFetchTrue() {
//        mainCoroutineRule.pauseDispatcher()

        feedViewModel.loadEvents(true)

//        assertThat(feedViewModel.isLoading).isTrue()
//        assertThat(LiveDataTestUtil.getValue(feedViewModel.swipeRefreshLoading)).isTrue()

        // Execute pending coroutines actions
//        mainCoroutineRule.resumeDispatcher()

        assertThat(feedViewModel.isLoading).isFalse()
        assertThat(LiveDataTestUtil.getValue(feedViewModel.swipeRefreshLoading)).isFalse()

        assertThat(LiveDataTestUtil.getValue(feedViewModel.events)).isEqualTo(events)
        assertThat(LiveDataTestUtil.getValue(feedViewModel.newEvents)).isNull()
    }

    @Test
    fun loadEvents_forceFetchFalse() {
//        mainCoroutineRule.pauseDispatcher()

        feedViewModel.loadEvents(false)

//        assertThat(feedViewModel.isLoading).isTrue()
//        assertThat(LiveDataTestUtil.getValue(feedViewModel.swipeRefreshLoading)).isTrue()

        // Execute pending coroutines actions
//        mainCoroutineRule.resumeDispatcher()

        assertThat(feedViewModel.isLoading).isFalse()
        assertThat(LiveDataTestUtil.getValue(feedViewModel.swipeRefreshLoading)).isFalse()

        assertThat(LiveDataTestUtil.getValue(feedViewModel.newEvents)).isEqualTo(events)
    }

    @Test
    fun loadEvents_error() {
        coreRemoteDataSource.events = null
//        mainCoroutineRule.pauseDispatcher()

        feedViewModel.loadEvents(false)

//        assertThat(feedViewModel.isLoading).isTrue()
//        assertThat(LiveDataTestUtil.getValue(feedViewModel.swipeRefreshLoading)).isTrue()

        // Execute pending coroutines actions
//        mainCoroutineRule.resumeDispatcher()

        assertThat(feedViewModel.isLoading).isFalse()
        assertThat(LiveDataTestUtil.getValue(feedViewModel.swipeRefreshLoading)).isFalse()

        assertThat(LiveDataTestUtil.getValue(feedViewModel.toastMessage)).isNotNull()
    }

    @Test
    fun loadScheduledEvents_forceFetchTrue() {
//        mainCoroutineRule.pauseDispatcher()

        scheduledEventsViewModel.loadEvents(true)

//        assertThat(feedViewModel.isLoading).isTrue()
//        assertThat(LiveDataTestUtil.getValue(feedViewModel.swipeRefreshLoading)).isTrue()

        // Execute pending coroutines actions
//        mainCoroutineRule.resumeDispatcher()

        assertThat(scheduledEventsViewModel.isLoading).isFalse()
        assertThat(LiveDataTestUtil.getValue(scheduledEventsViewModel.swipeRefreshLoading)).isFalse()

        assertThat(LiveDataTestUtil.getValue(scheduledEventsViewModel.events)).isEqualTo(scheduledEvents)
        assertThat(LiveDataTestUtil.getValue(scheduledEventsViewModel.newEvents)).isNull()
    }

    @Test
    fun loadScheduledEvents_forceFetchFalse() {
//        mainCoroutineRule.pauseDispatcher()

        scheduledEventsViewModel.loadEvents(false)

//        assertThat(feedViewModel.isLoading).isTrue()
//        assertThat(LiveDataTestUtil.getValue(feedViewModel.swipeRefreshLoading)).isTrue()

        // Execute pending coroutines actions
//        mainCoroutineRule.resumeDispatcher()

        assertThat(scheduledEventsViewModel.isLoading).isFalse()
        assertThat(LiveDataTestUtil.getValue(scheduledEventsViewModel.swipeRefreshLoading)).isFalse()

        assertThat(LiveDataTestUtil.getValue(scheduledEventsViewModel.newEvents)).isEqualTo(scheduledEvents)
    }

    @Test
    fun loadScheduledEvents_error() {
        coreRemoteDataSource.scheduledEvents = null
//        mainCoroutineRule.pauseDispatcher()

        scheduledEventsViewModel.loadEvents(false)

//        assertThat(scheduledEventsViewModel.isLoading).isTrue()
//        assertThat(LiveDataTestUtil.getValue(scheduledEventsViewModel.swipeRefreshLoading)).isTrue()

        // Execute pending coroutines actions
//        mainCoroutineRule.resumeDispatcher()

        assertThat(scheduledEventsViewModel.isLoading).isFalse()
        assertThat(LiveDataTestUtil.getValue(scheduledEventsViewModel.swipeRefreshLoading)).isFalse()

        assertThat(LiveDataTestUtil.getValue(scheduledEventsViewModel.toastMessage)).isNotNull()
    }

}