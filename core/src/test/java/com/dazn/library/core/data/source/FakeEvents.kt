package com.dazn.library.core.data.source

import com.dazn.library.core.data.Event
import java.text.SimpleDateFormat
import java.util.*

object FakeEvents {

    private val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())

    fun get() = listOf(
        Event("103", "Disney+", "United Kingdom", formatter.parse("2021-10-31T13:23:19.185Z")),
        Event("97", "Lenovo", "Laptop", formatter.parse("2021-10-23T13:23:19.185Z")),
        Event("100", "Amazon Prime", "United Kingdom", formatter.parse("2021-10-27T13:23:19.185Z")),
        Event("98", "SmartTV", "Sony", formatter.parse("2021-10-25T13:23:19.185Z")),
        Event("104", "DAZN", "United Kingdom", formatter.parse("2021-12-15T13:23:19.185Z")),
        Event("101", "Youtube", "Hong Kong", formatter.parse("2021-10-28T13:23:19.185Z")),
        Event("102", "Netflix", "Hong Kong", formatter.parse("2021-11-15T13:23:19.185Z")),
        Event("99", "Samsung", "Smart phone", formatter.parse("2021-10-26T13:23:19.185Z"))
    ).sortedBy { it.date }.toMutableList()

    fun getScheduled() = listOf(
        Event("103", "Disney+", "United Kingdom", formatter.parse("2021-10-31T13:23:19.185Z")),
        Event("97", "Lenovo", "Laptop", formatter.parse("2021-10-23T13:23:19.185Z")),
        Event("100", "Amazon Prime", "United Kingdom", formatter.parse("2021-10-27T13:23:19.185Z")),
        Event("98", "SmartTV", "Sony", formatter.parse("2021-10-25T13:23:19.185Z")),
        Event("99", "Samsung", "Smart phone", formatter.parse("2021-10-26T13:23:19.185Z"))
    ).sortedBy { it.date }.toMutableList()

}