package com.dazn.library.core.data.source

import com.dazn.library.core.data.Event
import com.dazn.library.core.network.Result
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import com.google.common.truth.Truth.assertThat

import java.text.SimpleDateFormat
import java.util.*

@ExperimentalCoroutinesApi
class CoreRepositoryTest {
    private val remoteEvents = FakeEvents.get()
    private val remoteScheduledEvents = FakeEvents.getScheduled()
    private lateinit var coreRemoteDataSource: FakeDataSource
    private lateinit var coreLocalDataSource: FakeDataSource
    //class under test
    private lateinit var coreRepository: CoreRepository

    @ExperimentalCoroutinesApi
    @Before
    fun createRepository() {
        coreRemoteDataSource = FakeDataSource(remoteEvents, remoteScheduledEvents)
        coreLocalDataSource = FakeDataSource()
        coreRepository = CoreRepository(coreLocalDataSource, coreRemoteDataSource)
    }

    @Test
    fun getEvents_requestAllFromRemoteDataSource() = runBlockingTest {
        val events = coreRepository.getEvents()
        assertThat(events.data).isEqualTo(remoteEvents)
    }

    @Test
    fun getEvents_unavailable_returnsError() = runBlockingTest {
        coreRemoteDataSource.events = null
        val events = coreRepository.getEvents()
        assertThat(events.status).isEqualTo(Result.Status.ERROR)
    }

    @Test
    fun getScheduledEvents_requestAllFromRemoteDataSource() = runBlockingTest {
        val events = coreRepository.getScheduledEvents()
        assertThat(events.data).isEqualTo(remoteScheduledEvents)
    }

    @Test
    fun getScheduledEvents_unavailable_returnsError() = runBlockingTest {
        coreRemoteDataSource.scheduledEvents = null
        val events = coreRepository.getScheduledEvents()
        assertThat(events.status).isEqualTo(Result.Status.ERROR)
    }


}