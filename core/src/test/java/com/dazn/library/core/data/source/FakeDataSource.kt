package com.dazn.library.core.data.source

import com.dazn.library.core.data.Event
import com.dazn.library.core.network.Result
import com.dazn.library.core.network.ResultError

class FakeDataSource(var events: MutableList<Event>? = mutableListOf(),
                     var scheduledEvents: MutableList<Event>? = mutableListOf()): CoreDataSource {

    override suspend fun getEvents(): Result<List<Event>> {
        events?.let { return Result.success(it) }
        return Result.error(ResultError("Events not found"))
    }

    override suspend fun getScheduledEvents(): Result<List<Event>> {
        scheduledEvents?.let { return Result.success(it) }
        return Result.error(ResultError("Events not found"))
    }
}